USE [GreenSlateDb]
GO
/****** Object:  Table [dbo].[Projects]    Script Date: 2/9/2019 8:09:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projects](
	[Id] [int] NOT NULL,
	[StartDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL,
	[Credits] [int] NOT NULL,
 CONSTRAINT [PK_Projects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserProjects]    Script Date: 2/9/2019 8:09:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProjects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ProjectId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_UserProjects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 2/9/2019 8:09:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Projects] ([Id], [StartDate], [EndDate], [Credits]) VALUES (1, CAST(N'2019-02-02T19:52:17.3471743' AS DateTime2), CAST(N'2019-02-16T19:52:17.3471743' AS DateTime2), 10)
GO
INSERT [dbo].[Projects] ([Id], [StartDate], [EndDate], [Credits]) VALUES (2, CAST(N'2019-02-08T19:52:17.3471739' AS DateTime2), CAST(N'2019-02-10T19:52:17.3471743' AS DateTime2), 20)
GO
INSERT [dbo].[Projects] ([Id], [StartDate], [EndDate], [Credits]) VALUES (3, CAST(N'2019-02-08T19:52:17.3471739' AS DateTime2), CAST(N'2019-02-09T19:52:17.3463457' AS DateTime2), 30)
GO
INSERT [dbo].[Projects] ([Id], [StartDate], [EndDate], [Credits]) VALUES (4, CAST(N'2019-02-02T19:52:17.3471743' AS DateTime2), CAST(N'2019-02-08T19:52:17.3471739' AS DateTime2), 40)
GO
INSERT [dbo].[Projects] ([Id], [StartDate], [EndDate], [Credits]) VALUES (5, CAST(N'2018-12-11T19:52:17.3471743' AS DateTime2), CAST(N'2019-02-09T19:52:17.3463457' AS DateTime2), 50)
GO
INSERT [dbo].[Projects] ([Id], [StartDate], [EndDate], [Credits]) VALUES (6, CAST(N'2018-12-11T19:52:17.3471743' AS DateTime2), CAST(N'2019-05-10T19:52:17.3471747' AS DateTime2), 10)
GO
INSERT [dbo].[Projects] ([Id], [StartDate], [EndDate], [Credits]) VALUES (7, CAST(N'2018-12-11T19:52:17.3471743' AS DateTime2), CAST(N'2019-02-10T19:52:17.3471743' AS DateTime2), 20)
GO
INSERT [dbo].[Projects] ([Id], [StartDate], [EndDate], [Credits]) VALUES (8, CAST(N'2019-02-02T19:52:17.3471743' AS DateTime2), CAST(N'2019-02-09T19:52:17.3463457' AS DateTime2), 30)
GO
INSERT [dbo].[Projects] ([Id], [StartDate], [EndDate], [Credits]) VALUES (9, CAST(N'2019-02-02T19:52:17.3471743' AS DateTime2), CAST(N'2019-02-10T19:52:17.3471743' AS DateTime2), 40)
GO
INSERT [dbo].[Projects] ([Id], [StartDate], [EndDate], [Credits]) VALUES (10, CAST(N'2019-02-02T19:52:17.3471743' AS DateTime2), CAST(N'2019-05-10T19:52:17.3471747' AS DateTime2), 50)
GO
INSERT [dbo].[Projects] ([Id], [StartDate], [EndDate], [Credits]) VALUES (11, CAST(N'2019-02-02T19:52:17.3471743' AS DateTime2), CAST(N'2019-05-10T19:52:17.3471747' AS DateTime2), 10)
GO
INSERT [dbo].[Projects] ([Id], [StartDate], [EndDate], [Credits]) VALUES (12, CAST(N'2019-05-10T19:52:17.3471747' AS DateTime2), CAST(N'2019-05-10T19:52:17.3471747' AS DateTime2), 20)
GO
INSERT [dbo].[Projects] ([Id], [StartDate], [EndDate], [Credits]) VALUES (13, CAST(N'2019-02-02T19:52:17.3471743' AS DateTime2), CAST(N'2019-05-10T19:52:17.3471747' AS DateTime2), 30)
GO
SET IDENTITY_INSERT [dbo].[UserProjects] ON 
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (1, 1, 1, 1, CAST(N'2019-02-02T19:52:17.3471743' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (2, 2, 2, 1, CAST(N'2019-02-16T19:52:17.3471743' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (3, 3, 3, 1, CAST(N'2019-05-10T19:52:17.3471747' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (4, 1, 4, 0, CAST(N'2019-02-09T19:52:17.3463457' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (5, 2, 5, 0, CAST(N'2019-02-08T19:52:17.3471739' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (6, 3, 6, 0, CAST(N'2019-02-10T19:52:17.3471743' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (7, 1, 7, 0, CAST(N'2019-02-02T19:52:17.3471743' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (8, 2, 8, 0, CAST(N'2018-12-11T19:52:17.3471743' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (9, 3, 9, 0, CAST(N'2019-02-09T19:52:17.3463457' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (10, 1, 10, 1, CAST(N'2019-05-10T19:52:17.3471747' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (11, 2, 11, 1, CAST(N'2019-02-16T19:52:17.3471743' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (12, 3, 12, 0, CAST(N'2019-02-08T19:52:17.3471739' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (13, 1, 12, 0, CAST(N'2019-02-02T19:52:17.3471743' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (14, 2, 12, 0, CAST(N'2019-02-02T19:52:17.3471743' AS DateTime2))
GO
INSERT [dbo].[UserProjects] ([Id], [UserId], [ProjectId], [IsActive], [AssignedDate]) VALUES (15, 3, 7, 0, CAST(N'2019-02-02T19:52:17.3471743' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[UserProjects] OFF
GO
INSERT [dbo].[Users] ([Id], [Name], [LastName]) VALUES (1, N'Carson', N'Alexander')
GO
INSERT [dbo].[Users] ([Id], [Name], [LastName]) VALUES (2, N'Meredith', N'Alonso')
GO
INSERT [dbo].[Users] ([Id], [Name], [LastName]) VALUES (3, N'Arturo', N'Anand')
GO
