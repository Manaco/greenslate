﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GreenslateMvcAngular.Models
{
    public class Project
    {
        public int UserId { get; set; }
        public int ProjectId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string TimeToStart { get; set; }
        public int Credits { get; set; }
        public bool Active { get; set; }
        public string ActiveMessage { get; set; }
        public DateTime AssignedDate { get; set; }
    }
}
