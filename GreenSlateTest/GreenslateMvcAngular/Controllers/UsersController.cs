﻿using System.Collections.Generic;
using System.Linq;
using GreenslateMvcAngular.BussinesLogicLayer;
using GreenslateMvcAngular.Models;
using Microsoft.AspNetCore.Mvc;

namespace GreenslateMvcAngular.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private IUsers Users { get; set; }
        private IUserProjects UserProjects { get; set; }

        public UsersController(IUsers _users, IUserProjects _userProjects)
        {
            Users = _users;
            UserProjects = _userProjects;
        }

        [HttpGet("[action]")]
        public List<User> GetUsers()
        {
            var usersFromBL = Users.GetUsers();
            var result = new List<User>();

            if (usersFromBL.Any())
            {   
                result.AddRange(
                    usersFromBL.Select(x => new User
                    {
                        Id = x.Id,
                        UserName = x.FullName
                    }).ToList());
            } 
            return result;
        }


        [HttpGet("[action]")]
        [Route("[action]/{userId}")]
        public List<Project> GetProjectsByUser(int userId)
        {
            var result = new List<Project>();
            var userProjects = UserProjects.GetProjectsByUser(userId);

            if (userProjects.Projects.Any())
            {
                result.AddRange(userProjects.Projects.Select(x => new Project {
                    ProjectId = x.ProjectId,
                    UserId = userId,
                    Active = x.IsActive,
                    ActiveMessage = x.ActiveMeesage,
                    AssignedDate = x.AssignedDate,
                    TimeToStart = x.TimeToStart,
                    Credits = x.Credits,
                    EndDate = x.EndDate,
                    StartDate = x.StartDate
                }));
            }

            return result;
        }

    }
}