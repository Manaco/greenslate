﻿using System;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using GreenslateMvcAngular.BussinesLogicLayer.Models;
using GreenslateMvcAngular.DataAccessLayer;
using Microsoft.EntityFrameworkCore;

namespace GreenslateMvcAngular.BussinesLogicLayer
{
    public class UserProjectsFromDapper : IUserProjects
    {
        private GreenSlateDbContext DataContext { get; set; }

        public UserProjectsFromDapper(GreenSlateDbContext _context)
        {
            DataContext = _context;
        }

        public UserProjects GetProjectsByUser(int userId)
        {
            try
            {
                UserProjects result = new UserProjects {
                    UserId = userId
                };

                string query = $@"Select 
	                                up.ProjectId,
	                                p.StartDate,
	                                p.EndDate,
	                                p.Credits,
	                                up.IsActive,
	                                up.AssignedDate
                                FROM 
	                                UserProjects up
                                INNER JOIN 
	                                Projects p on up.ProjectId = p.Id
                                WHERE
	                                up.UserId = { userId }";

                using (var connection = new SqlConnection(DataContext.Database.GetDbConnection().ConnectionString))
                {
                    var projects = connection.Query<Project>(query).ToList();
                    
                    if (projects.Any())
                    {
                        result.Projects.AddRange(projects.Select(x => new Project
                        {

                            IsActive = x.IsActive,
                            ProjectId = x.ProjectId,
                            ActiveMeesage = x.IsActive ? "Active" : "Inactive",
                            AssignedDate = x.AssignedDate,
                            Credits = x.Credits,
                            EndDate = x.EndDate,
                            Id = x.Id,
                            StartDate = x.StartDate,
                            TimeToStart = VerifyTimeToStart(x.StartDate, x.AssignedDate)
                        }));
                    }
                }
                    return result;
            }

            catch (Exception ex)
            {
                Console.WriteLine($"Exception to handle, detail: {ex.Message}");
                return new UserProjects();
            }

        }

        private string VerifyTimeToStart(DateTime startDate, DateTime assignedDate)
        {
            var days = (startDate - assignedDate).TotalDays;
            return (days <= 0) ? "Started" : Math.Abs(Math.Floor(days)).ToString();
        }
    }
}
