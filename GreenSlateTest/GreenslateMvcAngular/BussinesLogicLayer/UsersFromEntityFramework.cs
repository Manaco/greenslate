﻿using GreenslateMvcAngular.BussinesLogicLayer.Models;
using GreenslateMvcAngular.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GreenslateMvcAngular.BussinesLogicLayer
{
    public class UsersFromEntityFramework : IUsers
    {
        private GreenSlateDbContext DataContext{ get; set; }

        public UsersFromEntityFramework(GreenSlateDbContext _context)
        {
            DataContext = _context;
        }

        public IEnumerable<User> GetUsers()
        {
            try
            {
                var users = DataContext.Users.Select(x => new User
                {
                    Id = x.Id,
                    FullName = $"{x.LastName} , {x.Name}"
                });

                return users;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception to handle, detail: {ex.Message}");
                return new List<User>();         
            }
            
        }
     
    }
}
