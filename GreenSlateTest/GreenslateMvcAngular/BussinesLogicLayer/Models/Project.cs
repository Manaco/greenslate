﻿using System;

namespace GreenslateMvcAngular.BussinesLogicLayer.Models
{
    public class Project
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Credits { get; set; }
        public bool IsActive { get; set; }
        public string ActiveMeesage { get; set; } 
        public string TimeToStart { get; set; }
        public DateTime AssignedDate { get; set; }
    }
}
