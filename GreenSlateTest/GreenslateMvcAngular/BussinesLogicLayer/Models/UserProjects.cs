﻿using System.Collections.Generic;

namespace GreenslateMvcAngular.BussinesLogicLayer.Models
{
    public class UserProjects
    {
        public int UserId { get; set; }
        public List<Project> Projects { get; set; } = new List<Project>();
    }
}
