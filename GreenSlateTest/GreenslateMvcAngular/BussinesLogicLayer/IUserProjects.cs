﻿using GreenslateMvcAngular.BussinesLogicLayer.Models;

namespace GreenslateMvcAngular.BussinesLogicLayer
{
    public interface IUserProjects
    {
        UserProjects GetProjectsByUser(int userId);
    }
}
