﻿using GreenslateMvcAngular.BussinesLogicLayer.Models;
using GreenslateMvcAngular.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GreenslateMvcAngular.BussinesLogicLayer
{
    public interface IUsers
    {
        IEnumerable<User> GetUsers();
    }
}
