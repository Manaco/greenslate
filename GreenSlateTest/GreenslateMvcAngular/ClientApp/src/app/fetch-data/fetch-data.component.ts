import { Component, Inject, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html',
})

export class FetchDataComponent {

  public users: User[];
  public projects: Project[];

  http: HttpClient;
  baseUrl: string;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    this.http = http;
    this.baseUrl = baseUrl;

    http.get<User[]>(baseUrl + 'api/Users/GetUsers').subscribe(result => {
      this.users = result;  
    }, error => console.error(error));
    
  }

  selectedLevel;
  selected(e) {
    this.http.get<Project[]>(`${this.baseUrl}api/Users/GetProjectsByUser/${ e }`)
      .subscribe(result => {
        this.projects = result;
    }, error => console.error(error));
  }
}

interface User {
  id: number;
  userName: string;
}

interface Project {
  userid: number;
  projectId: number;
  startDate: Date;
  endDate: Date;
  credits: number;
  active: boolean;
  assignedDate: Date;
  timeToStart: number,
  activeMessage: string
}

