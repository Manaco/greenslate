﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreenslateMvcAngular.DataAccessLayer.Models;

namespace GreenslateMvcAngular.DataAccessLayer
{
    public class GreenSlateDbContext: DbContext
    {
        public GreenSlateDbContext(DbContextOptions<GreenSlateDbContext> options)
           : base(options)
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<UserProject> UserProjects { get; set; }
    }

}
