﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GreenslateMvcAngular.DataAccessLayer.Models
{
    public class UserProject
    {
        public int Id { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        [ForeignKey("Project")]
        public int ProjectId { get; set; }
        public bool IsActive { get; set; }
        public DateTime AssignedDate { get; set; }
    }
}
