﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GreenslateMvcAngular.DataAccessLayer.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var today = DateTime.Now;
            var yesterday = DateTime.Now.AddDays(-1);
            var tomorrow = DateTime.Now.AddDays(1);
            var aWeekAgo = DateTime.Now.AddDays(-7);
            var aWeekInFuture = DateTime.Now.AddDays(7);
            var twoMonthsAgo = DateTime.Now.AddDays(-60);
            var threeMonthsInTheFuture = DateTime.Now.AddDays(90);


            using (var context = new GreenSlateDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<GreenSlateDbContext>>()))
            {
                // Look for any movies.
                if (context.Users.Any())
                {
                    return;   // DB has been seeded
                }

                var users = new List<User>
                {
                    new User{Id = 1, Name="Carson",LastName="Alexander"},
                    new User{Id = 2, Name="Meredith",LastName="Alonso"},
                    new User{Id = 3, Name="Arturo",LastName="Anand"}
                };

                users.ForEach(s => context.Users.Add(s));
                context.SaveChanges();

                var projects = new List<Project>
                {
                    new Project{Id=1, StartDate = aWeekAgo, EndDate = aWeekInFuture, Credits=10},
                    new Project{Id=2, StartDate = yesterday, EndDate = tomorrow, Credits=20},
                    new Project{Id=3, StartDate = yesterday, EndDate = today, Credits=30},
                    new Project{Id=4, StartDate = aWeekAgo, EndDate = yesterday, Credits=40},
                    new Project{Id=5, StartDate = twoMonthsAgo, EndDate = today, Credits=50},
                    new Project{Id=6, StartDate = twoMonthsAgo, EndDate = threeMonthsInTheFuture, Credits=10},
                    new Project{Id=7, StartDate = twoMonthsAgo, EndDate = tomorrow, Credits=20},
                    new Project{Id=8, StartDate = aWeekAgo, EndDate = today, Credits=30},
                    new Project{Id=9, StartDate = aWeekAgo, EndDate = tomorrow, Credits=40},
                    new Project{Id=10, StartDate = aWeekAgo, EndDate = threeMonthsInTheFuture, Credits=50},
                    new Project{Id=11, StartDate = aWeekAgo, EndDate = threeMonthsInTheFuture, Credits=10},
                    new Project{Id=12, StartDate = threeMonthsInTheFuture, EndDate = threeMonthsInTheFuture, Credits=20},
                    new Project{Id=13, StartDate = aWeekAgo, EndDate = threeMonthsInTheFuture, Credits=30}

                };
                projects.ForEach(s => context.Projects.Add(s));
                context.SaveChanges();

            var userProjects = new List<UserProject>
            {
                new UserProject{UserId=1,ProjectId=1, AssignedDate= aWeekAgo, IsActive = true},
                new UserProject{UserId=2,ProjectId=2, AssignedDate= aWeekInFuture, IsActive = true},
                new UserProject{UserId=3,ProjectId=3, AssignedDate= threeMonthsInTheFuture, IsActive = true},
                new UserProject{UserId=1,ProjectId=4, AssignedDate= today, IsActive = false},
                new UserProject{UserId=2,ProjectId=5, AssignedDate= yesterday, IsActive = false},
                new UserProject{UserId=3,ProjectId=6, AssignedDate= tomorrow,IsActive = false},
                new UserProject{UserId=1,ProjectId=7, AssignedDate= aWeekAgo, IsActive = false},
                new UserProject{UserId=2,ProjectId=8, AssignedDate= twoMonthsAgo, IsActive = false},
                new UserProject{UserId=3,ProjectId=9, AssignedDate= today, IsActive = false},
                new UserProject{UserId=1,ProjectId=10, AssignedDate= threeMonthsInTheFuture, IsActive = true},
                new UserProject{UserId=2,ProjectId=11, AssignedDate= aWeekInFuture, IsActive = true},
                new UserProject{UserId=3,ProjectId=12, AssignedDate= yesterday, IsActive = false},
                new UserProject{UserId=1,ProjectId=12, AssignedDate= aWeekAgo, IsActive = false},
                new UserProject{UserId=2,ProjectId=12, AssignedDate= aWeekAgo, IsActive = false},
                new UserProject{UserId=3,ProjectId=7, AssignedDate= aWeekAgo, IsActive = false}
            };

                userProjects.ForEach(s => context.UserProjects.Add(s));
                context.SaveChanges();

            }
        }
    }
}
