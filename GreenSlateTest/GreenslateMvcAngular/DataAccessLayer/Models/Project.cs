﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GreenslateMvcAngular.DataAccessLayer.Models
{
    public class Project
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Credits { get; set; }
    }
}
